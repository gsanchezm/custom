package com.salesforce.props;

public class General {
    private String browserDriver;
    private String screenShot;

    public String getErrorException() {
        return errorException;
    }

    public void setErrorException(String errorException) {
        this.errorException = errorException;
    }

    private String errorException;

    public String getBrowserDriver() {
        return browserDriver;
    }

    public void setBrowserDriver(String browserDriver) {
        this.browserDriver = browserDriver;
    }


    public String getScreenShot() {
        return this.screenShot;
    }

    public void setScreenShot(String sScreenShot) {
        this.screenShot = sScreenShot;
    }

}
