package com.salesforce.utils;

import com.salesforce.config.BrowserType;
import com.salesforce.config.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.util.concurrent.TimeUnit;

public class DriverFactory {
    private DriverFactory() {

    }

    private static DriverFactory instance = new DriverFactory();

    public static DriverFactory getInstance() {
        return instance;
    }

    ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>() {
        @Override
        protected WebDriver initialValue() {
            return null;
        }
    };

    public WebDriver getDriver() {
        return driver.get();
    }

    public WebDriver setDriver(BrowserType browser) {
        String getOS = System.getProperty("os.name").toLowerCase();
        String osName = "";
        if (getOS.contains("mac")) {
            osName = "mac";
        }

        switch (browser.toString()) {
            case "CHROME":
                    System.setProperty("webdriver.chrome.driver", Constants.DRIVER_PATH + "chromedriver");
                driver.set(new ChromeDriver());
                break;
            case "FIREFOX":
                    System.setProperty("webdriver.gecko.driver", Constants.DRIVER_PATH + "geckodriver");
                driver.set(new FirefoxDriver());
                break;
            case "SAFARI":
                if (osName.equals("mac")) {
                    driver.set(new SafariDriver());
                }
                break;
        }
        int i = 10;

        driver.get().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        return driver.get();
    }


    public void removeDriver() {
        driver.get().quit();
        driver.remove();
    }
}
