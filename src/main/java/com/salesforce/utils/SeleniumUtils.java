package com.salesforce.utils;

import com.salesforce.config.Constants;
import com.salesforce.config.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SeleniumUtils {
    public static String SSDate;
    public static String SSDateTime;
    public static String file;

    /**
     * This function will take screenshot and highlight over element
     *
     * @param webdriver
     * @param snapshotError
     * @return
     * @throws IOException
     * @throws Exception
     */
    public static String takeSnapShot(WebDriver webdriver, WebElement element, String snapshotError) {
        SSDate = new SimpleDateFormat("yyyyMMdd_HH").format(Calendar.getInstance().getTime()).toString();
        SSDateTime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()).toString();
        file = Constants.SCREENSHOT_PATH + SSDate + "/" + snapshotError + SSDateTime + ".png";
        JavascriptExecutor js = (JavascriptExecutor) webdriver;
        js.executeScript("arguments[0].setAttribute('style','background: yellow')", element);
        //Convert web driver object to TakeScreenshot
        TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
        try { //Call getScreenshotAs method to create image file
            File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
            //Move image file to new destination
            File DestFile = new File(file);
            //Copy file at destination
            FileUtils.copyFile(SrcFile, DestFile);

            //***********************************************
//			ExcelUtils.hyperlinkScreenshot(file); --- Commented by space error on path Illegal character in path at index 17: C:/Users/gilberto sanchez/
            js.executeScript("arguments[0].setAttribute('style','background:')", element);
        } catch (Exception e) {
            Log.error("Class SeleniumUtils | Method takeSnapShot | Exception desc: " + e.getMessage());
        }
        return file;
    }

    /**
     * This function will take screenshot
     *
     * @param snapshotError
     * @return
     * @throws Exception
     */
    public static String takeSnapShot(String snapshotError) {
        SSDate = new SimpleDateFormat("yyyyMMdd_HH").format(Calendar.getInstance().getTime()).toString();
        SSDateTime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()).toString();
        file = Constants.SCREENSHOT_PATH + SSDate + "/" + snapshotError + SSDateTime + ".png";
        //Convert web driver object to TakeScreenshot
        TakesScreenshot scrShot = ((TakesScreenshot) DriverFactory.getInstance().getDriver());
        try { //Call getScreenshotAs method to create image file
            File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
            //Move image file to new destination
            File DestFile = new File(file);
            //Copy file at destination
            FileUtils.copyFile(SrcFile, DestFile);

            //***********************************************
//			ExcelUtils.hyperlinkScreenshot(file); --- Commented by space error on path Illegal character in path at index 17: C:/Users/gilberto sanchez/
        } catch (Exception e) {
            Log.error("Class SeleniumUtils | Method takeSnapShot | Exception desc: " + e.getMessage());
        }
        return file;
    }

    /**
     * HighLight an element
     *
     * @param driver
     * @param element
     * @throws InterruptedException
     */
    public static void fnHighlightMe(WebDriver driver, WebElement element) throws DriverException {
        //Creating JavaScriptExecuter Interface
        JavascriptExecutor js = (JavascriptExecutor) driver;
        for (int iCnt = 0; iCnt < 3; iCnt++) {
            //Execute javascript
            try {
                js.executeScript("arguments[0].setAttribute('style','background: yellow')", element);
                Thread.sleep(50);
                js.executeScript("arguments[0].setAttribute('style','background:')", element);
            } catch (InterruptedException e) {
                throw new DriverException("Class SeleniumUtils | Method fnHighlightMe | Exception desc: Exception", e);
            }
        }
    }
}
