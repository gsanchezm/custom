package com.salesforce.utils;
import org.openqa.selenium.support.Color;
import com.salesforce.config.BrowserType;
import com.salesforce.config.Log;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ActionKeywords extends Log {

    public static WebDriver driver;
    public static WebElement element;
    private static JavascriptExecutor js;
    private static WebDriverWait waitVar;
    static String pageLoadStatus = null;

    /**
     * Common functions/Actions to perform
     *
     * @param p
     * @param object
     * @param data
     * @return
     * @throws IOException
     * @throws Exception
     * @throws TimeoutException
     * @throws NoSuchElementException
     */

	/*####################################### Common Comands ############################################
     *
	 * Methods with all the common commands. 
	 * 
 	####################################################################################################*/

    //Open browser driver
    public boolean openBrowser(Properties p, String object, String data, String Link) throws DriverException, IOException {
        info("Opening Browser");
        try {
            switch (data.toLowerCase()) {
                case "firefox":
                    DriverFactory.getInstance().setDriver(BrowserType.FIREFOX);
                    info("Mozilla browser started");
                    break;
                case "chrome":
                    DriverFactory.getInstance().setDriver(BrowserType.CHROME);
                    info("Chrome browser started");
                    break;
                case "safari":
                    DriverFactory.getInstance().setDriver(BrowserType.SAFARI);
                    info("Safari browser started");
                    break;
                default:
                    break;
            }
            driver = DriverFactory.getInstance().getDriver();
            return true;
            //This block will execute only in case of an exception
        } catch (Exception e) {
            //Set the value of result variable to false
            new DriverException("Not able to open the selected browser.", e);
            return false;
        }
    }

    //Get url of application
    public boolean navigate(Properties p, String object, String data, String Link) throws DriverException {
        try {
            data = (data.startsWith("http://")) ? data : (data.startsWith("https://") ? data : "http://" + data);
            info("Navigating to URL: " + data);
            driver.get(data);
            do {
                js = (JavascriptExecutor) driver;
                pageLoadStatus = (String) js.executeScript("return document.readyState");
            } while (!pageLoadStatus.equals("complete"));
            return true;
        } catch (Exception e) {
            new DriverException("Not able to navigate.", e);
            return false;
        }
    }

    //Set text on control
    public static boolean input(Properties p, String object, String data, String Link) {
        try {
            info("Entering the text: " + data + " in " + object);
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            element.sendKeys(data);
            return true;
        } catch (Exception e) {
            new DriverException("Not able to type text.", e);
            return false;
        }
    }

    //Perform click
    public static boolean click(Properties p, String object, String data, String Link) {
        if (data.toLowerCase().equals("false")) {
            return true;
        }
        try {
            info("Clicking on Webelement " + object);
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            element.click();
            return true;
        } catch (Exception e) {
            new DriverException("Not able to click on element.", e);
            return false;
        }
    }

    //Clear text on control
    public static boolean clear(Properties p, String object, String data, String Link) {
        try {
            info("Cleaning: " + object);
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            element.clear();
            return true;
        } catch (Exception e) {
            new DriverException("Not able to clear text.", e);
            return false;
        }
    }

    //Close browser
    public boolean closeBrowser(Properties p, String object, String data, String Link) {
        try {
            info("Closing the browser");
            DriverFactory.getInstance().removeDriver();
            return true;
        } catch (Exception e) {
            new DriverException("Not able to close the browser", e);
            return false;
        }
    }

    //Refreshing the web page
    public static boolean refreshPage(Properties p, String object, String data, String Link) {
        try {
            info("Refreshing the page...");
            driver.navigate().refresh();
            do {
                js = (JavascriptExecutor) driver;
                pageLoadStatus = (String) js.executeScript("return document.readyState");
            } while (!pageLoadStatus.equals("complete"));
            return true;
        } catch (Exception e) {
            new DriverException("Not able to refresh the page", e);
            return false;
        }
    }

    //Wait
    public static boolean waitFor(Properties p, String object, String data, String Link) {
        try {
            double value_timedouble = Double.parseDouble(data);
            int value_time = (int) value_timedouble;
            Thread.sleep(value_time * 1000);
            info("Wait for " + data + " seconds");
            return true;
        } catch (TimeoutException | InterruptedException e) {
            new DriverException("Error waiting...", e);
            return false;
        }
    }

    //Explicit wait for all elements visible
    public static boolean waitForAllElementsVisible(Properties p, String object, String data, String Link) throws DriverException {
        try {
            waitVar = new WebDriverWait(driver, 20);
            info("Waiting for element: " + object);
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            waitVar.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(findElementBy(p, object)));
            return true;
        } catch (NoSuchElementException e) {
            new DriverException("Not element to wait.", e);
            return false;
        } catch (TimeoutException e) {
            new DriverException("Error waiting...", e);
            return false;
        }
    }

    //Explicit wait for element visible
    public static boolean waitForElementVisible(Properties p, String object, String data, String Link) throws DriverException {
        if(data.equals("")){
            return true;
        }
        try {
            waitVar = new WebDriverWait(driver, 20);
            info("Waiting for element: " + object);
            waitVar.until(ExpectedConditions.visibilityOfElementLocated(findElementBy(p, object)));
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            return true;
        } catch (NoSuchElementException e) {
            new DriverException("Not element to wait.", e);
            return false;
        } catch (TimeoutException e) {
            new DriverException("Error waiting...", e);
            return false;
        }
    }

    //Get text from text boxes
    public static boolean getTextBoxText(Properties p, String object, String data, String Link) {
        try {
            element = findElement(p, object);
            info("Getting the text: " + data + " from: " + object);
            SeleniumUtils.fnHighlightMe(driver, element);
            String text = element.getAttribute("value");
            if (data.equals("")) {
                info("Getting the text: " + text);
            } else if (!data.equals("") && data.equals(text)) {
                info("The text: " + text + " is equals to: " + data);
            } else if (!data.equals("") && !data.equals(text)) {
                info("The text: " + text + " is not equals to: " + data);
                new DriverException("The text: " + text + " is not equals to: " + data);
            }
            return true;
        } catch (Exception e) {
            new DriverException("Not able to get text.", e);
            return false;
        }
    }

    //You can use submit() on any element within the form, not just on the submit button itself.
    public static boolean submitForm(Properties p, String object, String data, String Link) {
        try {
            info("Form Submitted");
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            element.submit();
            return true;
        } catch (Exception e) {
            new DriverException("Not able to submit the form.", e);
            return false;
        }
    }

    //Execute JavaScript Code
    public static boolean executeJavaScript(Properties p, String object, String data, String Link) {
        try {
            info("Executing JavaScript");
            element = (p != null || !element.equals("")) ? findElement(p, object) : null;
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            if (element != null) {
                jse.executeScript(data, element);
            } else {
                jse.executeScript(data);
            }
            return true;
        } catch (Exception e) {
            new DriverException("Unable to execute JavaScript", e);
            return false;
        }
    }

    //Select element by text from drop down box
    public static boolean selectByTextFromList(Properties p, String object, String data, String Link) {
        try {
            if (!data.equals("")) {
                info("Selecting " + data + " from DropDown box: " + object);
                element = findElement(p, object);
                SeleniumUtils.fnHighlightMe(driver, element);
                new Select(findElement(p, object)).selectByVisibleText(data);
            }
            return true;
        } catch (Exception e) {
            new DriverException("Not able to select the element from dropdown.", e);
            return false;
        }
    }

    //Select element by value from drop down box
    public static boolean selectByValueFromList(Properties p, String object, String data, String Link) {
        try {
            info("Selecting " + data + " from DropDown box: " + object);
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            new Select(findElement(p, object)).selectByValue(data);
            return true;
        } catch (Exception e) {
            new DriverException("Not able to select the element from dropdown.", e);
            return false;
        }
    }

    //Select element by index from drop down box
    public static boolean selectByIndexFromList(Properties p, String object, String data, String Link) {
        try {
            info("Selecting " + data + " from DropDown box: " + object);
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            new Select(findElement(p, object)).selectByIndex(Integer.parseInt(data));
            return true;
        } catch (Exception e) {
            new DriverException("Not able to select the element from dropdown.", e);
            return false;
        }
    }

    //Get text from an element
    public static boolean getElementText(Properties p, String object, String data, String Link) {
        if(data.equals("")){
            return true;
        }
        try {
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            if (data.isEmpty()) {
                info("Getting the text: " + element.getText() + " from " + object);
            } else if (!data.equals("") && element.getText().equals(data)) {
                info(element.getText() + " is equal to: " + data);
            } else {
                new DriverException(element.getText() + " is not equal to: " + data);
                return false;
            }
            return true;
        } catch (Exception e) {
            new DriverException("Something went wrong getting element text", e);
            return false;
        }
    }

    //Move mouse over an specified element
    public static boolean moveToElement(Properties p, String object, String data, String Link) {
        try {
            info("Moving mouse to Element " + object);
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            Actions builder2 = new Actions(driver);
            Action mouseOver = builder2
                    .moveToElement(element)
                    .build();
            mouseOver.perform();
            return true;
        } catch (Exception e) {
            new DriverException("Not able to move to element defined.", e);
            return false;
        }
    }

    //####################################################################################################

    /*##################################### Customized Methods ###########################################
     *
     * Methods created for specific functionality.
     *
     ####################################################################################################*/
    //Clicking on item using the text
    public static boolean clickOnTextItem(Properties p, String object, String data, String Link) {
        try {
            if (!data.equals("")) {
                try {
                    element = returnXpath(p, ".//*[text()='" + data + "']");
                } catch (NoSuchElementException ne) {
                    element = returnXpath(p, ".//*[contains(text(),'" + data + "')]");
                }

                SeleniumUtils.fnHighlightMe(driver, element);
                Log.info("Clicking on element: " + element.getText());
                element.click();
            }
            return true;
        } catch (Exception e) {
            new DriverException("No item " + data + " found", e);
            return false;
        }
    }

    public static String getColor(Properties p, String object, String data, String Link) {
        String color = driver.findElement(By.xpath(object)).getCssValue("background-color");
        System.out.println(color);
        String hex = Color.fromString(color).asHex();
        System.out.println(hex);
        return color;
    }

    //Get css value from an element
    public static boolean getElementCSS(Properties p, String object, String data, String Link) {
        try {
            element = findElement(p, object);
            SeleniumUtils.fnHighlightMe(driver, element);
            info("Getting the value: " + element.getCssValue(data) + " from " + object);
            return true;
        } catch (Exception e) {
            new DriverException("Something went wrong getting element css value", e);
            return false;
        }
    }



    //####################################################################################################


    private static WebElement findElement(Properties p, String locator) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        String objLocator = (p != null) ? (p.getProperty(locator) != null ? p.getProperty(locator) : locator) : locator;
        try {
            return driver.findElement(By.id(objLocator));
        } catch (NoSuchElementException e) {
            return returnElementName(objLocator);
        }
    }

    private static WebElement returnElementName(String name) {
        try {
            return driver.findElement(By.name(name));
        } catch (NoSuchElementException e) {
            return returnElementXpath(name);
        }
    }

    private static WebElement returnElementXpath(String xpath) {
        try {
            return driver.findElement(By.xpath(xpath));
        } catch (NoSuchElementException e) {
            return returnElementCssSelector(xpath);
        }
    }

    private static WebElement returnElementCssSelector(String cssSelector) {
        try {
            return driver.findElement(By.cssSelector(cssSelector));
        } catch (NoSuchElementException e) {
            return returnElementClassName(cssSelector);
        }
    }

    private static WebElement returnElementClassName(String className) {
        try {
            return driver.findElement(By.className(className));
        } catch (NoSuchElementException e) {
            return returnElementLinkText(className);
        }
    }

    private static WebElement returnElementLinkText(String linkText) {
        try {
            return driver.findElement(By.partialLinkText(linkText));
        } catch (NoSuchElementException e) {
            return returnElementTagname(linkText);
        }
    }

    private static WebElement returnElementTagname(String tagName) {
        try {
            return driver.findElement(By.tagName(tagName));
        } catch (NoSuchElementException ne) {
            /*try {
                throw new DriverException("Class ActionKeywords | Method findElement | Exception desc: Can not find the element: " + tagName);
            } catch (DriverException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            DriverScript.bResult = false;*/
            return null;
        }
    }

    private static By findElementBy(Properties p, String locator) {
        String objLocator = (p != null) ? (p.getProperty(locator) != null ? p.getProperty(locator) : locator) : locator;
        try {
            driver.findElement(By.id(objLocator));
            return By.id(objLocator);
        } catch (NoSuchElementException e) {
            return returnElementByName(objLocator);
        }
    }

    private static By returnElementByName(String name) {
        try {
            driver.findElement(By.name(name));
            return By.name(name);
        } catch (NoSuchElementException e) {
            return returnElementByXpath(name);
        }
    }

    private static By returnElementByXpath(String xpath) {
        try {
            driver.findElement(By.xpath(xpath));
            return By.xpath(xpath);
        } catch (NoSuchElementException e) {
            return returnElementByCssSelector(xpath);
        }
    }

    private static By returnElementByCssSelector(String cssSelector) {
        try {
            driver.findElement(By.cssSelector(cssSelector));
            return By.cssSelector(cssSelector);
        } catch (NoSuchElementException e) {
            return returnElementByClassName(cssSelector);
        }
    }

    private static By returnElementByClassName(String className) {
        try {
            driver.findElement(By.className(className));
            return By.className(className);
        } catch (NoSuchElementException e) {
            return returnElementByLinkText(className);
        }
    }

    private static By returnElementByLinkText(String linkText) {
        try {
            driver.findElement(By.partialLinkText(linkText));
            return By.partialLinkText(linkText);
        } catch (NoSuchElementException e) {
            return returnElementByTagname(linkText);
        }
    }

    private static By returnElementByTagname(String tagName) {
        try {
            driver.findElement(By.tagName(tagName));
            return By.tagName(tagName);
        } catch (NoSuchElementException ne) {
           /* try {
                throw new DriverException("Class ActionKeywords | Method findElementBy | Exception desc: Can not find the element: " + tagName);
            } catch (DriverException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            DriverScript.bResult = false;*/
            return null;
        }
    }

    //Returns only Xpath
    private static WebElement returnXpath(Properties p, String xpath) {
        String objLocator = (p != null) ? (p.getProperty(xpath) != null ? p.getProperty(xpath) : xpath) : xpath;
        return driver.findElement(By.xpath(objLocator));
    }

}