package com.salesforce.config;

public enum BrowserType {
    CHROME,
    FIREFOX,
    SAFARI,
    REMOTE
}
